import 'package:flutter/material.dart';

class Answers extends StatelessWidget {
  Function fun; // increase
  String textVal;
  int index_answer;
  Answers({this.fun, this.textVal, this.index_answer});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: RaisedButton(
          color: Colors.blue,
          child: Text(
            textVal,
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () {
            fun(index_answer);
          },
        ),
      ),
    );
  }
}
