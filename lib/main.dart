import 'package:flutter/material.dart';
import 'package:max_quiz_app/answers.dart';
import 'package:max_quiz_app/questions.dart';
import 'package:max_quiz_app/quiz.dart';
import 'package:max_quiz_app/result.dart';

void main() => runApp(MyApp()); //start run

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

////////////////////////////////////////////////////////////////////////////
class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int index = 0;
  List<Map> values = [
    {
      "questionVal": "What is your Favourite color ?",
      "Answers": [
        {"text": "RED", "score": 30},
        {"text": "Green", "score": 20},
        {"text": "BLUE", "score": 20},
        {"text": "BLACK", "score": 30}
      ]
    }, //100
    {
      "questionVal": "What is your name?",
      "Answers": [
        {"text": "Salama", "score": 50},
        {"text": "Ahmed", "score": 30},
        {"text": "Omar", "score": 20},
        {"text": "Ramy", "score": 50},
        {"text": "Yassen", "score": 30},
        {"text": "Hamada", "score": 20}
      ]
    }, //200
    {
      "questionVal": "What is your age?",
      "Answers": [
        {"score": 20, "text": "20"},
        {"score": 30, "text": "30"},
        {"score": 40, "text": "40"},
        {"score": 50, "text": "100"}
      ]
    },
  ];
  int scorees = 0;
  void increase(int indexs) {
    scorees += values[index]["Answers"][indexs]["score"]; //errror404
    print(scorees);
    index++;
    setState(() {});
  }

  void reset() {
    scorees = 0;
    index = 0;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    // here start run state
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Quiz App",
            style: TextStyle(
                fontSize: 20.0,
                color: Colors.white,
                fontWeight: FontWeight.bold),
          ),
        ),
        body: index < values.length
            ? Quiz(index: index, increase: increase, values: values)
            : Result(
                scores: scorees,
                fun: reset,
              ));
  }
}
