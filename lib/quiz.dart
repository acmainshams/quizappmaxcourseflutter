import 'package:flutter/material.dart';
import 'package:max_quiz_app/answers.dart';
import 'package:max_quiz_app/questions.dart';

class Quiz extends StatelessWidget {
  Function increase;
  List<Map> values;
  List<Map> answers;

  int index;
  Quiz({this.increase, this.index, this.values});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Questions(
          questions: values[index]["questionVal"],
        ),
        for (int r = 0; r < values[index]["Answers"].length; r++)
          Answers(
              fun: increase,
              textVal: values[index]["Answers"][r]["text"],
              index_answer: r),
      ],
    );
  }
}
