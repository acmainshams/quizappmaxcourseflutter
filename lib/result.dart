import 'package:flutter/material.dart';
import 'package:max_quiz_app/main.dart';
import 'package:max_quiz_app/quiz.dart';

class Result extends StatelessWidget {
  int scores;
  Function fun;
  Result({this.scores, this.fun});
  String result() {
    return "YOU DID IT !";
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 50.0,
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          child: Text(
            result() + " : " + scores.toString(),
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.blue.shade900, fontSize: 30),
          ),
        ),
        FlatButton(
            onPressed: () {
              fun();
            },
            child: Text(
              "Resset Exam",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold),
            ))
      ],
    );
  }
}
